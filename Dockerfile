FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 443

# install System.Drawing native dependencies
RUN apt-get update && apt-get install -y --allow-unauthenticated libgdiplus libc6-dev libgdiplus libx11-dev && rm -rf /var/lib/apt/lists/*

# Set the locale
RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen de_DE.UTF-8  
ENV LANG de_DE.UTF-8  
ENV LANGUAGE de_DE:de  
ENV LC_ALL de_DE.UTF-8 

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["DabesiaWebApp/DabesiaWebApp.csproj", "DabesiaWebApp/"]
RUN dotnet restore "DabesiaWebApp/DabesiaWebApp.csproj"
COPY . .
WORKDIR "/src/DabesiaWebApp"
RUN dotnet build "DabesiaWebApp.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "DabesiaWebApp.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "DabesiaWebApp.dll"]