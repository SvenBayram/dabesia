﻿using DabesiaWebApp.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DabesiaWebApp.Controllers
{
    public class AccountsController : Controller
    {
        private readonly DabesiaWebAppContext _context;

        public AccountsController(DabesiaWebAppContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Login()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginUser user)
        {
            if (LoginUser(user.UserName, user.Password))
            {
                List<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName)
                };

                ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "login");

                ClaimsPrincipal claimPrincipal = new ClaimsPrincipal(userIdentity);

                AuthenticationProperties authProperties = new AuthenticationProperties
                {
                    AllowRefresh = true,
                    IsPersistent = true,
                    ExpiresUtc = DateTimeOffset.Now.AddMinutes(15)
                };

                await AuthenticationHttpContextExtensions.SignInAsync(HttpContext, claimPrincipal, authProperties);
                return RedirectToAction("Index", "Home");
            }
            user.LoginErrorMessage = "Benutzername oder Passwort falsch!";
            return View("Login", user);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await AuthenticationHttpContextExtensions.SignOutAsync(HttpContext);
            HttpContext.Session.Clear();
            return RedirectToAction("Login", "Accounts");
        }

        [HttpGet]
        public new IActionResult Unauthorized()
        {
            return View();
        }

        private bool LoginUser(string username, string password)
        {
            var userDetails = _context.LoginUser.Where(x => x.UserName == username && x.Password == password).FirstOrDefault();
            if (userDetails == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}