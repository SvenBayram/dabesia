﻿using DabesiaWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Controllers
{
    [Authorize]
    public class CustomersController : Controller
    {
        private readonly DabesiaWebAppContext _context;

        public CustomersController(DabesiaWebAppContext context)
        {
            _context = context;
        }

        // GET: Kunden
        public async Task<IActionResult> Index(string searchString, string sortOrder)
        {
            IEnumerable<Customer> customers = await _context.Customer.Include(k => k.PurchaseOrders).ToListAsync();

            SearchCustomers(customers, searchString);

            return View(SortCustomers(customers, sortOrder));
        }

        // GET: Kunden/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kunde = await _context.Customer
                .SingleOrDefaultAsync(m => m.CustomerID == id);
            if (kunde == null)
            {
                return NotFound();
            }

            return View(kunde);
        }

        // GET: Kunden/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Kunden/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FirstName,LastName,Street,Number,Zip,City,Phonenumber,Email")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                int maxValue = await _context.Customer.MaxAsync(k => k.CustomerID);
                int nextValue = (maxValue % 10000) + ((DateTime.Today.Year - 2000) * 10000) + 1;
                customer.CustomerID = nextValue;
                _context.Add(customer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // GET: Kunden/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer.SingleOrDefaultAsync(m => m.CustomerID == id);
            if (customer == null)
            {
                return NotFound();
            }
            return View(customer);
        }

        // POST: Kunden/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CustomerID,FirstName,LastName,Street,Number,Zip,City,Phonenumber,Email")] Customer customer)
        {
            if (id != customer.CustomerID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(customer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(customer.CustomerID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(customer);
        }

        // GET: Kunden/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer
                .SingleOrDefaultAsync(m => m.CustomerID == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Kunden/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var customer = await _context.Customer.SingleOrDefaultAsync(m => m.CustomerID == id);
            _context.Customer.Remove(customer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Kunden/Orders/5
        public async Task<IActionResult> CustomerOrders(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var customer = await _context.Customer.Include("PurchaseOrders.Product")
                .SingleOrDefaultAsync(m => m.CustomerID == id);
            if (customer == null)
            {
                return NotFound();
            }
            return View(customer);
        }

        private bool CustomerExists(int id)
        {
            return _context.Customer.Any(e => e.CustomerID == id);
        }

        private IOrderedEnumerable<Customer> SortCustomers(IEnumerable<Customer> customerList, string sortOrder)
        {
            ViewBag.CustomerSortParm = String.IsNullOrEmpty(sortOrder) ? "CustomerDesc" : "";
            ViewBag.LastNameSortParm = sortOrder == "LastName" ? "LastNameDesc" : "LastName";
            ViewBag.FirstNameSortParm = sortOrder == "FirstName" ? "FirstNameDesc" : "FirstName";

            switch (sortOrder)
            {
                case "CustomerDesc":
                    return customerList.OrderByDescending(c => c.CustomerID);
                    break;
                case "LastName":
                    return customerList.OrderBy(c => c.LastName);
                    break;
                case "LastNameDesc":
                    return customerList.OrderByDescending(c => c.LastName);
                    break;
                case "FirstName":
                    return customerList.OrderBy(c => c.FirstName);
                    break;
                case "FirstNameDesc":
                    return customerList.OrderByDescending(c => c.FirstName);
                    break;
                default:
                    return customerList.OrderBy(c => c.CustomerID);
                    break;
            }
        }

        private void SearchCustomers(IEnumerable<Customer> customers, string searchString)
        {
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                customers = customers.Where(s =>
                    s.CustomerID.ToString().ToLower().Contains(searchString.ToLower()) ||
                    s.FirstName.ToLower().Contains(searchString.ToLower()) ||
                    s.LastName.ToLower().Contains(searchString.ToLower()));
                ViewBag.SearchString = searchString;
            }
        }

    }
}
