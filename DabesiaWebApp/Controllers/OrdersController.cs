﻿using DabesiaWebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly DabesiaWebAppContext _context;

        public OrdersController(DabesiaWebAppContext context)
        {
            _context = context;
        }

        // GET: Bestellungen
        public async Task<IActionResult> Index(string sortOrder, string searchString, string orderState, DateTime? startDate, DateTime? endDate)
        {
            ViewBag.OrderSortParm = string.IsNullOrEmpty(sortOrder) ? "OrderDesc" : "";
            ViewBag.StateSortParm = sortOrder == "State" ? "StateDesc" : "State";
            ViewBag.DateSortParm = sortOrder == "Date" ? "DateDesc" : "Date";
            ViewBag.CustomerSortParm = sortOrder == "Customer" ? "CustomerDesc" : "Customer";

            IEnumerable<PurchaseOrder> orders = await _context.PurchaseOrder.Include(b => b.Customer).Include(b => b.Product).ToListAsync();

            if (!string.IsNullOrWhiteSpace(searchString))
            {
                orders = orders.Where(b =>
                    b.Customer.LastName.ToLower().Contains(searchString.ToLower()) ||
                    b.Customer.FirstName.ToLower().Contains(searchString.ToLower()) ||
                    b.OrderID.ToString().ToLower().Contains(searchString.ToLower()));
                ViewBag.SearchString = searchString;
            }

            if (!string.IsNullOrWhiteSpace(orderState))
            {
                if (orderState != "Alle")
                {
                    orders = orders.Where(b => b.OrderState.ToString() == orderState);
                }
            }

            if (startDate != null)
            {
                if (endDate == null)
                {
                    endDate = DateTime.Today;
                }

                if (startDate <= endDate)
                {
                    orders = orders.Where(b => (b.OrderDate >= startDate && b.OrderDate <= endDate));
                }
                else
                {
                    orders = orders.Where(b => (b.OrderDate <= startDate && b.OrderDate >= endDate));
                }

                ViewBag.StartDate = startDate.Value.Day + "." + startDate.Value.Month + "." + startDate.Value.Year;
                ViewBag.EndDate = endDate.Value.Day + "." + endDate.Value.Month + "." + endDate.Value.Year;
            }

            IOrderedEnumerable<PurchaseOrder> ordersSorted = orders.OrderBy(b => b.OrderID);

            switch (sortOrder)
            {
                case "OrderDesc":
                    ordersSorted = orders.OrderByDescending(b => b.OrderID);
                    break;
                case "State":
                    ordersSorted = orders.OrderBy(b => b.OrderState);
                    break;
                case "StateDesc":
                    ordersSorted = orders.OrderByDescending(b => b.OrderState);
                    break;
                case "Date":
                    ordersSorted = orders.OrderBy(b => b.OrderDate);
                    break;
                case "DateDesc":
                    ordersSorted = orders.OrderByDescending(b => b.OrderDate);
                    break;
                case "Customer":
                    ordersSorted = orders.OrderBy(b => b.Customer.LastName);
                    break;
                case "CustomerDesc":
                    ordersSorted = orders.OrderByDescending(b => b.Customer.LastName);
                    break;
                default:
                    break;
            }

            return View(ordersSorted);
        }

        // GET: Bestellungen/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.PurchaseOrder
                .Include(b => b.Customer)
                .Include(b => b.Product)
                .SingleOrDefaultAsync(m => m.OrderID == id);

            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Bestellungen/Create
        public IActionResult Create(int? id)
        {
            if (id.HasValue)
            {
                ViewBag.Customer = id;
            }

            ViewData["CustomerID"] = new SelectList(_context.Customer, "CustomerID", "CustomerID");
            ViewData["ProductID"] = new SelectList(_context.Product, "ProductID", "ProductID");

            ViewBag.Today = DateTime.Today.Day + "." + DateTime.Today.Month + "." + DateTime.Today.Year;

            return View();
        }

        // POST: Bestellungen/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("OrderDate,CustomerID,ProductID,OrderState,PaymentDate,UsageDate,Discount,VoucherCode,ExpiryDate")] PurchaseOrder order)
        {
            if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewData["CustomerID"] = new SelectList(_context.Customer, "CustomerID", "CustomerID", order.CustomerID);
            ViewData["ProductID"] = new SelectList(_context.Product, "ProductID", "Description", order.ProductID);

            return View(order);
        }

        // GET: Bestellungen/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.PurchaseOrder.SingleOrDefaultAsync(m => m.OrderID == id);

            if (order == null)
            {
                return NotFound();
            }

            ViewBag.OrderDate = order.OrderDate;

            ViewData["CustomerID"] = new SelectList(_context.Customer, "CustomerID", "CustomerID", order.CustomerID);
            ViewData["ProductID"] = new SelectList(_context.Product, "ProductID", "Description", order.ProductID);

            HttpContext.Session.SetInt32("StatusCache", (int)order.OrderState);

            return View(order);
        }

        // POST: Bestellungen/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("OrderID,OrderDate,CustomerID,ProductID,OrderState,PaymentDate,UsageDate,Discount,VoucherCode,ExpiryDate")] PurchaseOrder order)
        {
            if (id != order.OrderID)
            {
                return NotFound();
            }

            OrderState StatusCache = (OrderState)HttpContext.Session.GetInt32("StatusCache");

            if (StatusCache == OrderState.Offen && order.OrderState == OrderState.Bezahlt)
            {
                order.PaymentDate = DateTime.Today.Date;
            }

            if (StatusCache == OrderState.Bezahlt && order.OrderState == OrderState.Abgeschlossen)
            {
                order.UsageDate = DateTime.Today.Date;
            }

            if (StatusCache == OrderState.Abgeschlossen && order.OrderState == OrderState.Offen)
            {
                order.UsageDate = null;
                order.PaymentDate = null;
            }

            if (StatusCache == OrderState.Bezahlt && order.OrderState == OrderState.Offen)
            {
                order.PaymentDate = null;
            }

            HttpContext.Session.Remove("StatusCache");

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.OrderID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            ViewData["CustomerID"] = new SelectList(_context.Customer, "CustomerID", "CustomerID", order.CustomerID);
            ViewData["ProductID"] = new SelectList(_context.Product, "ProductID", "ProductID", order.ProductID);

            return View(order);
        }

        // GET: Bestellungen/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.PurchaseOrder
                .Include(b => b.Customer)
                .Include(b => b.Product)
                .SingleOrDefaultAsync(m => m.OrderID == id);

            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Bestellungen/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.PurchaseOrder.SingleOrDefaultAsync(m => m.OrderID == id);

            _context.PurchaseOrder.Remove(order);

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> ExtendExpiryDate(int id)
        {
            PurchaseOrder order = await _context.PurchaseOrder.SingleOrDefaultAsync(m => m.OrderID == id);

            order.ExpiryDate = order.ExpiryDate.Value.AddYears(1);

            await _context.SaveChangesAsync();

            return RedirectToAction("Details", "Orders", new { id });
        }

        private bool OrderExists(int id)
        {
            return _context.PurchaseOrder.Any(e => e.OrderID == id);
        }
    }
}
