﻿using DabesiaWebApp.Helpers;
using DabesiaWebApp.Models;
using iText.IO.Image;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Windows;

namespace DabesiaWebApp.Controllers
{
    [Authorize]
    public class FileController : Controller
    {
        private readonly DabesiaWebAppContext _context;

        public FileController(DabesiaWebAppContext context)
        {
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> DownloadVoucher(int id)
        {
            PurchaseOrder order = await _context.PurchaseOrder.Include(b => b.Product).Include(b => b.Customer).SingleOrDefaultAsync(m => m.OrderID == id);

            Voucher voucher = new Voucher();
            voucher.Create(order.OrderID);
            voucher.AddContent(order);
            voucher.Save();

            var memory = new MemoryStream();
            using (var stream = new FileStream(voucher.FilePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return File(memory, "application/pdf", Path.GetFileName(voucher.FilePath));
        }

        [HttpPost]
        public async Task<IActionResult> DownloadInvoice(int id)
        {
            PurchaseOrder order = await _context.PurchaseOrder.Include(b => b.Product).Include(b => b.Customer).SingleOrDefaultAsync(m => m.OrderID == id);

            Invoice invoice = new Invoice();
            invoice.Create(order.OrderID);
            invoice.AddContent(order);
            invoice.Save();

            var memory = new MemoryStream();
            using (var stream = new FileStream(invoice.FilePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;

            return File(memory, "application/vnd.ms-word", Path.GetFileName(invoice.FilePath));
        }
    }
}