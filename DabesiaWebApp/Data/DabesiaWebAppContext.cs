﻿using DabesiaWebApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Models
{
    public class DabesiaWebAppContext : DbContext
    {
        public DabesiaWebAppContext(DbContextOptions<DabesiaWebAppContext> options)
            : base(options)
        {
        }

        public DbSet<Customer> Customer { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<PurchaseOrder> PurchaseOrder { get; set; }

        public DbSet<LoginUser> LoginUser { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PurchaseOrder>().HasOne(b => b.Product).WithMany(p => p.PurchaseOrders).HasForeignKey(b => b.ProductID);
            modelBuilder.Entity<PurchaseOrder>().HasOne(b => b.Customer).WithMany(k => k.PurchaseOrders).HasForeignKey(b => b.CustomerID);
        }
    }
}
