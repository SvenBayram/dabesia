﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DabesiaWebApp.Models
{
    public class DataStoreContext
    {
        public string ConnectionString { get; set; }

        public DataStoreContext(string connectionString)
        {
            ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }

        public List<Kunde> GetAllKunden()
        {
            List<Kunde> KundenListe = new List<Kunde>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM kunde", conn);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        KundenListe.Add(new Kunde()
                        {
                            KundenNr = Convert.ToInt32(reader["KundenNr"]),
                            Vorname = reader["Vorname"].ToString(),
                            Nachname = reader["Nachname"].ToString(),
                            Plz = Convert.ToInt32(reader["Plz"]),
                            Strasse = reader["Strasse"].ToString(),
                            Ort = reader["Ort"].ToString(),
                            Hausnummer= reader["Hausnummer"].ToString()
                        });
                    }
                }
            }
            return KundenListe;
        }

        public List<Produkt> GetAllProdukte()
        {
            List<Produkt> ProduktListe = new List<Produkt>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM produkt", conn);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ProduktListe.Add(new Produkt()
                        {
                            ProduktNr = Convert.ToInt32(reader["ProduktNr"]),
                            Bezeichnung = reader["Bezeichnung"].ToString(),
                            Preis = Convert.ToDecimal(reader["Preis"]),
                            Bemerkung = reader["Bemerkung"].ToString()
                        });
                    }
                }
            }
            return ProduktListe;
        }

        public List<Gutschein> GetAllGutscheine()
        {
            List<Gutschein> GutscheinListe = new List<Gutschein>();
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM gutschein", conn);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        GutscheinListe.Add(new Gutschein()
                        {
                            GutscheinNr = Convert.ToInt32(reader["GutscheinNr"]),
                            GutscheinCode = reader["GutscheinCode"].ToString()
                        });
                    }
                }
            }
            return GutscheinListe;
        }
    }
}
