﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Models
{
    public enum OrderState
    {
        Offen = 1,
        Bezahlt,
        Abgeschlossen,
        Abgelaufen
    }

    public class PurchaseOrder
    {
        [Key]
        [Display(Name = "BestellNr")]
        public int OrderID { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Bestelldatum")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? OrderDate { get; set; }

        [Display(Name = "KundenNr")]
        public int CustomerID { get; set; }
        public Customer Customer { get; set; }

        [Display(Name = "ProduktNr")]
        public int ProductID { get; set; }
        public Product Product { get; set; }

        [Display(Name = "Status")]
        public OrderState OrderState { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Bezahlungdatum")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PaymentDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Verwendungsdatum")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? UsageDate { get; set; }

        [Display(Name = "Preisnachlass")]
        [DisplayFormat(DataFormatString = "{0:n} €")]
        [RegularExpression(@"^(\d+(?:[\,]\d{2})?)$", ErrorMessage = "Der eingegebene Wert ist nicht zulässig!")]
        public string Discount { get; set; }

        [Display(Name = "Code")]
        public string VoucherCode { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Ablaufdatum")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? ExpiryDate { get; set; }
    }
}
