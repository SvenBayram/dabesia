﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Models
{
    public class LoginUser
    {
        [Key]
        public int UserID { get; set; }

        [Display(Name = "Benutzername:")]
        [Required(ErrorMessage = "Dieses Feld wird benötigt!")]
        public string UserName { get; set; }

        [Display(Name = "Passwort:")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Dieses Feld wird benötigt!")]
        public string Password { get; set; }

        [NotMapped]
        public string LoginErrorMessage { get; set; }
    }
}
