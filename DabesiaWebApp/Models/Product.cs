﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Models
{
    public class Product
    {
        [Key]
        [Display(Name = "Produkt-Nr")]
        public int ProductID { get; set; }

        [Display(Name = "Produktbezeichnung")]
        public string Description { get; set; }

        [Display(Name = "Produktbeschreibung")]
        public string Commentary { get; set; }

        [Display(Name = "Preis")]
        [DisplayFormat(DataFormatString = "{0:n} €")]
        [RegularExpression(@"^(\d+(?:[\,]\d{2})?)$", ErrorMessage = "Der eingegebene Wert ist nicht zulässig!")]
        public string Price { get; set; }

        public List<PurchaseOrder> PurchaseOrders { get; set; }
    }
}
