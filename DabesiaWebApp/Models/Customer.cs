﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Models
{
    public class Customer
    {
        [Key]
        [Display(Name = "Kunden-Nr")]
        public int CustomerID { get; set; }

        [Display(Name = "Vorname")]
        public string FirstName { get; set; }

        [Display(Name = "Nachname")]
        public string LastName { get; set; }

        [Display(Name = "Strasse")]
        public string Street { get; set; }

        [Display(Name = "Hausnummer")]
        public string Number { get; set; }

        [Display(Name = "Plz")]
        public int Zip { get; set; }

        [Display(Name = "Ort")]
        public string City { get; set; }

        [Display(Name = "Telefonnummer")]
        public string Phonenumber { get; set; }

        public string Email { get; set; }

        public List<PurchaseOrder> PurchaseOrders { get; set; }
    }
}
