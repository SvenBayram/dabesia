﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Helpers
{
    public abstract class WordDocument
    {
        protected void AddText(WordprocessingDocument document, string bookmarkName, string text)
        {
            foreach (BookmarkStart bookmarkStart in document.MainDocumentPart.RootElement.Descendants<BookmarkStart>())
            {
                if (bookmarkStart.Name == bookmarkName)
                {
                    Run bookmarkText = bookmarkStart.NextSibling<Run>();
                    if (bookmarkText != null)
                    {
                        bookmarkText.GetFirstChild<Text>().Text = text;
                    }
                }
            }
        }
        protected void AddImage(WordprocessingDocument document, string imageId, Bitmap newImage)
        {
            ImagePart imagePart = (ImagePart)document.MainDocumentPart.GetPartById(imageId);
            byte[] imageBytes;
            using (MemoryStream stream = new MemoryStream())
            {
                newImage.Save(stream, ImageFormat.Bmp);
                imageBytes = stream.ToArray();
            }
            BinaryWriter writer = new BinaryWriter(imagePart.GetStream());
            writer.Write(imageBytes);
            writer.Close();
        }
    }
}
