﻿using DabesiaWebApp.Models;
using DocumentFormat.OpenXml.Packaging;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Helpers
{
    public class Voucher : WordDocument
    {
        private WordprocessingDocument voucher;
        public string FilePath { get; private set; }
        public void Create(int orderId)
        {
            FilePath = Path.Combine("wwwroot", "files", "voucher", "Gutschein_" + orderId.ToString("D4") + ".docx");
            File.Copy(Path.Combine("wwwroot", "files", "voucher", "VoucherTemplate.docx"), FilePath, true);
            voucher = WordprocessingDocument.Open(FilePath, true);
        }
        public void AddContent(PurchaseOrder order)
        {
            string pilot = "Pilot: " + order.Customer.FirstName + " " + order.Customer.LastName;
            string dauer = order.Product.Commentary;
            string expireDate = order.ExpiryDate.Value.Day.ToString("D2") + "." + order.ExpiryDate.Value.Month.ToString("D2") + "." + order.ExpiryDate.Value.Year.ToString("D4");
            string voucherCode = order.VoucherCode;

            AddText(voucher, "Pilot", pilot);
            AddText(voucher, "Dauer", dauer);
            AddText(voucher, "Gutschein_Ablaufdatum", expireDate);
            AddText(voucher, "Gutschein_Code", voucherCode);

            Bitmap qrCode = GenerateQR(voucherCode);
            AddImage(voucher, "rId5", qrCode);
        }
        public void Save()
        {
            voucher.Save();
            voucher.Close();
        }
        private Bitmap GenerateQR(string voucherCode)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(voucherCode, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            return qrCode.GetGraphic(20);
        }
    }
}
