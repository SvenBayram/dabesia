﻿using iText.IO.Image;
using iText.Kernel.Pdf;
using iText.Layout;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Helpers
{
    public static class Extensions
    {
        public static string ToFinancialNotation(this double value)
        {
            double roundedValue = Math.Round(value, 2, MidpointRounding.AwayFromZero);
            roundedValue = Math.Truncate(roundedValue * 100) / 100;
            return string.Format("{0:N2}", roundedValue);
        }
    }
}
