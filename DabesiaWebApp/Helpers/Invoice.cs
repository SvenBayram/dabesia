﻿using DabesiaWebApp.Models;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DabesiaWebApp.Helpers
{
    public class Invoice : WordDocument
    {
        private WordprocessingDocument invoice;
        public string FilePath { get; private set; }
        public void Create(int orderId)
        {
            FilePath = Path.Combine("wwwroot", "files", "invoice", "Rechnung_" + orderId.ToString("D4") + ".docx");
            File.Copy(Path.Combine("wwwroot", "files", "invoice", "InvoiceTemplate.docx"), FilePath, true);
            invoice = WordprocessingDocument.Open(FilePath, true);
        }

        public void AddContent(PurchaseOrder order)
        {
            string name = order.Customer.FirstName + " " + order.Customer.LastName;
            string street = order.Customer.Street + " " + order.Customer.Number;
            string city = order.Customer.Zip + " " + order.Customer.City;
            string land = "Deutschland";
            string invoiceId = order.OrderDate.Value.Year + "-" + order.OrderID.ToString("D4");
            string customerId = order.CustomerID.ToString();
            string invoiceDate = order.OrderDate.Value.Day.ToString("D2") + "." + order.OrderDate.Value.Month.ToString("D2") + "." + order.OrderDate.Value.Year.ToString("D4");
            string productDescription = order.Product.Description;

            double price = Convert.ToDouble(order.Product.Price);
            double fee = CalculateFee(price);
            double sum = price + fee;

            AddText(invoice, "Kunde_Name", name);
            AddText(invoice, "Kunde_Strasse", street);
            AddText(invoice, "Kunde_Ort", city);
            AddText(invoice, "Kunde_Land", land);
            AddText(invoice, "Rechnung_Nr", invoiceId);
            AddText(invoice, "Kunde_Nr", customerId);
            AddText(invoice, "Rechnung_Datum", invoiceDate);
            AddText(invoice, "Produkt_Bezeichnung", productDescription);
            AddText(invoice, "Produkt_Einzelpreis", price.ToFinancialNotation());
            AddText(invoice, "Rechnung_Gesamtpreis", price.ToFinancialNotation());
            AddText(invoice, "Rechnung_Nettobetrag", price.ToFinancialNotation());
            AddText(invoice, "Rechnung_Mehrwertsteuer", fee.ToFinancialNotation());
            AddText(invoice, "Rechnung_Gesamtbetrag", sum.ToFinancialNotation());

            if (order.OrderState == OrderState.Bezahlt)
            {
                AddText(invoice, "Rechnung_Bezahlinfo", "Die Rechnung über " + sum.ToFinancialNotation() + " Euro wurde bereits per PAYPAL beglichen.");
            }
            else
            {
                AddText(invoice, "Rechnung_Bezahlinfo", "Die Rechnung über " + sum.ToFinancialNotation() + " Euro wurde noch nicht beglichen.");
            }
        }

        public void Save()
        {
            invoice.Save();
            invoice.Close();
        }

        private double CalculateFee(double sum)
        {
            return sum *= 0.19;
        }
    }
}
