-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 09. Okt 2018 um 09:40
-- Server-Version: 10.1.35-MariaDB
-- PHP-Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `dabesia`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `customer`
--

CREATE TABLE `customer` (
  `CustomerID` int(11) NOT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `Street` varchar(50) DEFAULT NULL,
  `Number` varchar(10) DEFAULT NULL,
  `Zip` int(11) DEFAULT NULL,
  `City` varchar(50) DEFAULT NULL,
  `Phonenumber` varchar(15) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `customer`
--

INSERT INTO `customer` (`CustomerID`, `FirstName`, `LastName`, `Street`, `Number`, `Zip`, `City`, `Phonenumber`, `Email`) VALUES
(170001, 'Mark', 'Meier', 'Kirchenstrasse', '23a', 13876, 'Bernau', '01560856949', 'dvdotnet@aol.com'),
(170002, 'Uwe', 'Bach', 'Hauptstrasse', '7', 45852, 'Anschau', '01744084399', 'scottzed@live.com'),
(170003, 'John', 'Schmidt', 'Gartenstrasse', '3', 52831, 'Koblenz', '01497896315', 'parksh@hotmail.com'),
(180001, 'Tim', 'Herrmann', 'Neuendorferstrasse', '123', 97454, 'Rudow', '01237545974', 'yangyan@yahoo.com'),
(180002, 'James', 'Mustermann', 'Neustrasse', '56b', 12545, 'Spandau', '04053992708', 'ilyaz@att.net'),
(180003, 'Jimmy', 'Jimjim', 'Falkenseerstrasse', '897', 45612, 'Neukölln', '08015110154', 'leakin@sbcglobal.net'),
(180004, 'Markus', 'Luther', 'Bergstrasse', '31', 23513, 'Kreuzberg', '06261522312', 'maradine@yahoo.ca');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `loginuser`
--

CREATE TABLE `loginuser` (
  `UserID` int(11) NOT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `loginuser`
--

INSERT INTO `loginuser` (`UserID`, `UserName`, `Password`) VALUES
(1, 'Admin', 'Password');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `product`
--

CREATE TABLE `product` (
  `ProductID` int(11) NOT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Price` varchar(10) DEFAULT NULL,
  `Commentary` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `product`
--

INSERT INTO `product` (`ProductID`, `Description`, `Price`, `Commentary`) VALUES
(1, 'Simulatorflug MiG21-30', '84,00', '30-minütigen'),
(2, 'Simulatorflug MiG21-60', '155,00', '1-stündigen'),
(3, 'Simulatorflug MiG21-90', '220,00', '90-minütigen');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `purchaseorder`
--

CREATE TABLE `purchaseorder` (
  `OrderID` int(11) NOT NULL,
  `OrderDate` date DEFAULT NULL,
  `CustomerID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `OrderState` int(5) NOT NULL DEFAULT '1',
  `PaymentDate` date DEFAULT NULL,
  `UsageDate` date DEFAULT NULL,
  `Discount` varchar(10) DEFAULT '0,00',
  `VoucherCode` varchar(50) DEFAULT NULL,
  `ExpiryDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `purchaseorder`
--

INSERT INTO `purchaseorder` (`OrderID`, `OrderDate`, `CustomerID`, `ProductID`, `OrderState`, `PaymentDate`, `UsageDate`, `Discount`, `VoucherCode`, `ExpiryDate`) VALUES
(1, '2016-03-11', 170001, 2, 3, '2016-03-11', '2016-07-16', '30,00', 'CS4C-EKJ8-2JK9-8ZE1', '2020-03-12'),
(2, '2016-05-15', 170002, 3, 4, '2016-05-15', NULL, '0,00', 'GH67-USDD-E5VE-R5EF', '2018-05-16'),
(3, '2017-04-12', 170003, 1, 3, '2017-04-12', '2017-11-18', '0,00', 'QWE2-WDF8-4RDV-GB4T', '2019-04-13'),
(4, '2017-10-25', 180001, 1, 3, '2017-10-27', '2017-11-27', '20,00', 'EF1C-T781-HEAS-DFC7', '2019-10-26'),
(5, '2018-02-12', 180002, 2, 3, '2018-02-12', '2018-04-19', '0,00', 'CX58-EWE4-5175-V4HR', '2020-02-13'),
(6, '2018-04-25', 180003, 1, 2, '2018-04-25', '2018-06-22', '0,00', 'DF4R-EWV4-Z1XC-851J', '2020-04-26'),
(7, '2018-06-21', 180004, 1, 1, NULL, NULL, '0,00', 'ASD1-5C5T-1ZDR-0V2A', '2020-06-22');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`CustomerID`);

--
-- Indizes für die Tabelle `loginuser`
--
ALTER TABLE `loginuser`
  ADD PRIMARY KEY (`UserID`);

--
-- Indizes für die Tabelle `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ProductID`);

--
-- Indizes für die Tabelle `purchaseorder`
--
ALTER TABLE `purchaseorder`
  ADD PRIMARY KEY (`OrderID`),
  ADD KEY `FkCustomerID` (`CustomerID`),
  ADD KEY `FkProductID` (`ProductID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `loginuser`
--
ALTER TABLE `loginuser`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `product`
--
ALTER TABLE `product`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `purchaseorder`
--
ALTER TABLE `purchaseorder`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `purchaseorder`
--
ALTER TABLE `purchaseorder`
  ADD CONSTRAINT `FkCustomerID` FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FkProductID` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
