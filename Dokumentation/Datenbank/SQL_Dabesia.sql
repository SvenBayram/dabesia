CREATE TABLE Customer
(
    CustomerID INT NOT NULL,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    Street VARCHAR(50),
    Number VARCHAR(10),
    Zip INT,
    City VARCHAR(50),
    Phonenumber VARCHAR(15),
    Email VARCHAR(50),
    PRIMARY KEY (CustomerID)
);

CREATE TABLE Product
(
    ProductID INT NOT NULL AUTO_INCREMENT,
    Description VARCHAR(50),
    Price VARCHAR(10),
    Commentary VARCHAR(200),
    PRIMARY KEY (ProductID)
);

CREATE TABLE PurchaseOrder
(
    OrderID INT NOT NULL AUTO_INCREMENT,
    OrderDate date,
    CustomerID INT NOT NULL,
    ProductID INT NOT NULL,
    OrderState INT(5) NOT NULL DEFAULT '1',
    PaymentDate date,
    UsageDate date,
    Discount VARCHAR(10) DEFAULT '0,00',
    VoucherCode VARCHAR(50),
    ExpiryDate date,
    PRIMARY KEY (OrderID)
);

CREATE TABLE LoginUser
(
    UserID INT NOT NULL AUTO_INCREMENT,
    UserName VARCHAR(50),
    Password VARCHAR(100),
    PRIMARY KEY (UserID)
);

ALTER TABLE PurchaseOrder
ADD CONSTRAINT FkCustomerID FOREIGN KEY (CustomerID) REFERENCES Customer (CustomerID) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE PurchaseOrder
ADD CONSTRAINT FkProductID FOREIGN KEY (ProductID) REFERENCES Product (ProductID) ON UPDATE CASCADE ON DELETE NO ACTION;



INSERT INTO Customer(CustomerID, FirstName, LastName, Street, Number, Zip, City, Phonenumber, Email)
VALUES
    ('170001', 'Mark',   'Meier',       'Kirchenstrasse',     '23a',  13876, 'Bernau',    '01560856949',  'dvdotnet@aol.com'),
    ('170002', 'Uwe',    'Bach',        'Hauptstrasse',       '7',    45852, 'Anschau',   '01744084399',  'scottzed@live.com'),
    ('170003', 'John',   'Schmidt',     'Gartenstrasse',      '3',    52831, 'Koblenz',   '01497896315',  'parksh@hotmail.com'),
    ('180001', 'Tim',    'Herrmann',    'Neuendorferstrasse', '123',  97454, 'Rudow',     '01237545974',  'yangyan@yahoo.com'),
    ('180002', 'James',  'Mustermann',  'Neustrasse',         '56b',  12545, 'Spandau',   '04053992708',  'ilyaz@att.net'),
    ('180003', 'Jimmy',  'Jimjim',      'Falkenseerstrasse',  '897',  45612, 'Neukölln',  '08015110154',  'leakin@sbcglobal.net'),
    ('180004', 'Markus', 'Luther',      'Bergstrasse',        '31',   23513, 'Kreuzberg', '06261522312',  'maradine@yahoo.ca');
    
INSERT INTO Product(Description, Price, Commentary)
VALUES
    ('Simulatorflug MiG21-30', '84,00',  '30-minütigen'),
    ('Simulatorflug MiG21-60', '155,00', '1-stündigen'),
    ('Simulatorflug MiG21-90', '220,00', '90-minütigen');
    
INSERT INTO PurchaseOrder(OrderDate, CustomerID, ProductID, OrderState, PaymentDate, UsageDate, Discount, VoucherCode, ExpiryDate)
VALUES
    ('2016-3-11',  170001, 2, 3, '2016-3-11',  '2016-7-16',  '30,00', 'CS4C-EKJ8-2JK9-8ZE1', '2018-3-12' ),
    ('2016-5-15',  170002, 3, 4, '2016-5-15',   NULL,        '0,00',  'GH67-USDD-E5VE-R5EF', '2018-5-16' ),
    ('2017-4-12',  170003, 1, 3, '2017-4-12',  '2017-11-18', '0,00',  'QWE2-WDF8-4RDV-GB4T', '2019-4-13' ),
    ('2017-10-25', 180001, 1, 3, '2017-10-27', '2017-11-27', '20,00', 'EF1C-T781-HEAS-DFC7', '2019-10-26'),
    ('2018-2-12',  180002, 2, 3, '2018-2-12',  '2018-4-19',  '0,00',  'CX58-EWE4-5175-V4HR', '2020-2-13' ),
    ('2018-4-25',  180003, 1, 2, '2018-4-25',  '2018-6-22',  '0,00',  'DF4R-EWV4-Z1XC-851J', '2020-4-26' ),
    ('2018-6-21',  180004, 1, 1,  NULL,         NULL,        '0,00',  'ASD1-5C5T-1ZDR-0V2A', '2020-6-22' );

INSERT INTO LoginUser(UserName, Password)
VALUES
    ('Admin','Password');